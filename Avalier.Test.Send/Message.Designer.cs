﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Avalier.Test.Send {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Message {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Message() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Avalier.Test.Send.Message", typeof(Message).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///&lt;PerformanceAdjusted xmlns=&quot;http://www.mesa.org/xml/B2MML-BHPBilliton&quot;&gt;
        ///	&lt;ApplicationArea&gt;
        ///		&lt;CreationDateTime&gt;2016-04-04T03:20:00.001Z&lt;/CreationDateTime&gt;
        ///		&lt;BODID&gt;1d92ca1cfa1411e592100af0591000001d92cc56fa1411e5&lt;/BODID&gt;
        ///	&lt;/ApplicationArea&gt;
        ///	&lt;DataArea&gt;
        ///		&lt;Notify&gt;
        ///			&lt;ActionCriteria&gt;
        ///				&lt;ActionExpression actionCode=&quot;Notify&quot;/&gt;
        ///			&lt;/ActionCriteria&gt;
        ///		&lt;/Notify&gt;
        ///		&lt;Changed effectiveTimestamp=&quot;2016-03-01T01:00:00Z&quot; recordTimestamp=&quot;2016-04-04T03:20:00.001Z&quot;&gt;
        ///			&lt;MaterialLot&gt;
        ///				&lt;ID&gt;dc476ca0-1005- [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string PerformanceAdjusted_BlastBlock_InventoryBalances_Work_Performance {
            get {
                return ResourceManager.GetString("PerformanceAdjusted_BlastBlock_InventoryBalances_Work_Performance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///&lt;PerformanceAdjusted xmlns=&quot;http://www.mesa.org/xml/B2MML-BHPBilliton&quot;&gt;
        ///	&lt;ApplicationArea&gt;
        ///		&lt;CreationDateTime&gt;2016-04-04T03:20:00.001Z&lt;/CreationDateTime&gt;
        ///		&lt;BODID&gt;1d9062ccfa1411e592100af0591000001d9064fcfa1411e5&lt;/BODID&gt;
        ///	&lt;/ApplicationArea&gt;
        ///	&lt;DataArea&gt;
        ///		&lt;Notify&gt;
        ///			&lt;ActionCriteria&gt;
        ///				&lt;ActionExpression actionCode=&quot;Notify&quot;/&gt;
        ///			&lt;/ActionCriteria&gt;
        ///		&lt;/Notify&gt;
        ///		&lt;Changed effectiveTimestamp=&quot;2016-03-01T01:00:00Z&quot; recordTimestamp=&quot;2016-04-04T03:20:00.001Z&quot;&gt;
        ///			&lt;MaterialLot&gt;
        ///				&lt;ID&gt;04022016DS&lt;/ID [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string PerformanceAdjusted_PreCrusher_InventoryBalances_Work_Performance {
            get {
                return ResourceManager.GetString("PerformanceAdjusted_PreCrusher_InventoryBalances_Work_Performance", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to 
        ///&lt;PerformanceTracked xmlns=&quot;http://www.mesa.org/xml/B2MML-BHPBilliton&quot;&gt;
        ///	&lt;ApplicationArea&gt;
        ///		&lt;CreationDateTime&gt;2016-04-04T03:19:59Z&lt;/CreationDateTime&gt;
        ///		&lt;BODID&gt;1d5d7de4fa1411e592100af0591000001d5d7f92fa1411e5&lt;/BODID&gt;
        ///	&lt;/ApplicationArea&gt;
        ///	&lt;DataArea&gt;
        ///		&lt;Notify&gt;
        ///			&lt;ActionCriteria&gt;
        ///				&lt;ActionExpression actionCode=&quot;Notify&quot;/&gt;
        ///			&lt;/ActionCriteria&gt;
        ///		&lt;/Notify&gt;
        ///		&lt;Added effectiveTimestamp=&quot;2016-03-01T01:00:00Z&quot; recordTimestamp=&quot;2016-04-04T03:19:59Z&quot;&gt;
        ///			&lt;WorkPerformance&gt;
        ///				&lt;ID&gt;MQ2.12154179&lt;/ID&gt;
        ///		 [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string PerformanceTracked_Blastblock_Precrusher_Movement_Work_Performance {
            get {
                return ResourceManager.GetString("PerformanceTracked_Blastblock_Precrusher_Movement_Work_Performance", resourceCulture);
            }
        }
    }
}
