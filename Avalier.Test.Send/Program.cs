﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Avalier.Test.Contracts;

namespace Avalier.Test.Send
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var connection = new Avalier.Nms.Messaging.ActiveMQ.Connection("tcp://localhost:61616", "admin", "admin")
                .AddSender("Avalier.Test", sender => sender.Scan(Assembly.Load("Avalier.Test.Contracts")));

            var numberOfMessages = 10;

            using (var bus = connection.Start())
            {
                var startedAt = DateTime.UtcNow;
                for (var i = 0; i < numberOfMessages; i++)
                {
                    var command = new TextCommand()
                    {
                        Text = Message.PerformanceTracked_Blastblock_Precrusher_Movement_Work_Performance
                    };
                    bus.Send(command);
                    //bus.Send("Avalier.Test", "{\"Text\":\"Hello World\"}");
                }
                var endedAt = DateTime.UtcNow;

                var elapsed = (long)endedAt.Subtract(startedAt).TotalMilliseconds;

                Console.WriteLine($"{numberOfMessages} messages sent in {elapsed} milliseconds");
                Console.WriteLine("Press <enter> to continue...");
                Console.ReadLine();
            }

        }
    }
}
