﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Nms.Messaging
{
    public static class Magic
    {
        public static string Namespace = typeof(Magic).Assembly.GetName().Name;

        public static class MessageHeader
        {
            public static string Client = Namespace + ":Client";
            public static string Verb = Namespace + ":Verb";
            public static string Action = Namespace + ":Action";
        }
    }
}
