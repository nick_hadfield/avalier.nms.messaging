﻿using System;
using System.Collections.Generic;
using Avalier.Nms.Messaging.Base;

namespace Avalier.Nms.Messaging
{
    public interface IConnection : IDisposable
    {
        string ClientName { get; }
        Func<Type, object> Resolver { get; }
        IList<Base.Receiver> Receivers { get; }
        IList<Base.Sender> Senders { get; }
        Base.Sender GetSenderByQueue(string queue);
    }

    public interface IConnection<TReceiver, TSender> : IConnection
        where TReceiver : Base.Receiver
        where TSender : Base.Sender
    {
        IConnection<TReceiver, TSender> SetResolver(Func<Type, object> resolver);
        IConnection<TReceiver, TSender> AddSender(string queueName, Action<TSender> senderConfiguration = null);
        IConnection<TReceiver, TSender> AddReceiver(string queueName, Action<TReceiver> receiverConfiguration = null);
        IBus Start();
    }
}