﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Nms.Messaging
{
    public interface IReceiver
    {
        string QueueName { get; }
        IReceiver Register(Type notificationType, object handler = null);
        IReceiver ScanForHandlers(Assembly assembly = null);
        void Initialise();
    }
}
