﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Nms.Messaging
{
    public interface IBus : IDisposable
    {
        void Send(string queueName, string text);

        void Send<TCommand>(TCommand command)
            where TCommand : class;

        /*/
        void Publish<TNotification>(TNotification notification)
            where TNotification : class;
        /*/

        /*/
        TResponse Request<TRequest, TResponse>(TRequest request)
            where TRequest : class
            where TResponse : class;
        /*/
    }
}
