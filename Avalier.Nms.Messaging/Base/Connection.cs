﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Nms.Messaging.Base
{
    public abstract class Connection<TReceiver, TSender> : IConnection<TReceiver, TSender>
        where TReceiver : Base.Receiver
        where TSender : Base.Sender
    {
        private readonly IDictionary<string, TSender> _sendersByQueue;
        private readonly IList<TReceiver> _receivers;

        protected Connection(string clientId = "")
        {
            _sendersByQueue = new Dictionary<string, TSender>();
            _receivers = new List<TReceiver>();
            this.ClientId = clientId;
        }

        public string ClientId { get; }

        public string ClientName
        {
            get
            {
                var name = $"{Environment.MachineName}:{Assembly.GetEntryAssembly().GetName().Name}";
                if (!string.IsNullOrEmpty(this.ClientId))
                {
                    name += $":{this.ClientId}";
                }
                return name;
            }
        }

        protected IDictionary<string, TSender> SendersByQueue => _sendersByQueue;

        protected IList<TReceiver> Receivers => _receivers;

        public Base.Sender GetSenderByQueue(string queue)
        {
            if (this.SendersByQueue.ContainsKey(queue))
            {
                return (Base.Sender)this.SendersByQueue[queue];
            }
            return null;
        }

        IList<Base.Sender> IConnection.Senders => _sendersByQueue.Values.Cast<Base.Sender>().ToList();

        IList<Base.Receiver> IConnection.Receivers => _receivers.Cast<Base.Receiver>().ToList();

        private Func<Type, object> _resolver = null;

        public Func<Type, object> Resolver => _resolver ?? Activator.CreateInstance;

        public IConnection<TReceiver, TSender> SetResolver(Func<Type, object> resolver)
        {
            _resolver = resolver;
            return this;
        }

        public abstract IConnection<TReceiver, TSender> AddSender(string queueName, Action<TSender> senderConfiguration = null);

        public abstract IConnection<TReceiver, TSender> AddReceiver(string queueName, Action<TReceiver> receiverConfiguration = null);

        public abstract IBus Start();

        public abstract void Dispose();
    }
}
