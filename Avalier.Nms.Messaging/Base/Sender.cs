﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Nms.Messaging.Base
{
    public abstract class Sender
    {                                                
        private readonly IList<Type> _registeredMessageTypes;

        protected Sender(string queueName)
        {
            _registeredMessageTypes = new List<Type>();
            this.QueueName = queueName.ThrowIfNull(nameof(queueName));
        }

        public string QueueName { get; }

        public Sender Register<TMessage>()
        {
            return this.Register(typeof(TMessage));
        }

        private Sender Register(Type messageType)
        {
            if (!_registeredMessageTypes
                .Where(t => t == messageType)
                .Any()
            )
            {
                _registeredMessageTypes.Add(messageType);
            }
            return this;
        }

        protected bool Supports<TMessage>()
        {
            return _registeredMessageTypes.Contains(typeof(TMessage));
        }

        public Sender Scan(Assembly assembly = null)
        {
            assembly = assembly ?? Assembly.GetCallingAssembly();
            this.ScanForNotifications(assembly);
            this.ScanForCommands(assembly);
            this.ScanForRequests(assembly);
            return this;
        }

        public Sender ScanForNotifications(Assembly assembly = null)
        {
            assembly = assembly ?? Assembly.GetCallingAssembly();
            var messageTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Notification"))
                .ToList();

            foreach (var messageType in messageTypes)
            {
                this.Register(messageType);
            }

            return this;
        }

        public Sender ScanForCommands(Assembly assembly = null)
        {
            assembly = assembly ?? Assembly.GetCallingAssembly();
            var messageTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Command"))
                .ToList();

            foreach (var messageType in messageTypes)
            {
                this.Register(messageType);
            }

            return this;
        }

        public Sender ScanForRequests(Assembly assembly = null)
        {
            assembly = assembly ?? Assembly.GetCallingAssembly();
            var messageTypes = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Where(t => t.Name.EndsWith("Request"))
                .ToList();

            foreach (var messageType in messageTypes)
            {
                this.Register(messageType);
            }

            return this;
        }

        public abstract void Initialise();

        public abstract void Dispose();

        public abstract void Send(string text);

        public abstract void Send<TCommand>(TCommand command);

    }
}
