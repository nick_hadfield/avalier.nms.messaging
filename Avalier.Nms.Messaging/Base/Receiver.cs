﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Avalier.Nms.Messaging.Base
{
    public abstract class Receiver : IReceiver, IDisposable
    {
        private readonly IDictionary<Type, IList<object>> _handlers;
        private readonly IConnection _connection;

        protected Receiver(IConnection connection, string queueName)
        {
            _handlers = new Dictionary<Type, IList<object>>();
            _connection = connection.ThrowIfNull(nameof(connection));
            this.QueueName = queueName.ThrowIfNull(nameof(queueName));
        }

        public string QueueName { get; }

        public IReceiver Register(Type notificationType, object handler = null)
        {
            if (!_handlers.ContainsKey(notificationType))
            {
                _handlers[notificationType] = new List<object>();
            }
            if (null != handler)
            {
                _handlers[notificationType].Add(handler);
            }
            return this;
        }

        public IReceiver ScanForHandlers(Assembly assembly = null)
        {
            assembly = assembly ?? Assembly.GetCallingAssembly();
            var handlerDefinitions = assembly.GetTypes()
                .Where(t => t.IsClass)
                .Select(t => {
                    foreach (var i in t.GetInterfaces())
                    {
                        if (i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IHandler<>))
                        {
                            return new
                            {
                                HandlerType = t,
                                HandlerInterface = i,
                                NotificationType = i.GetGenericArguments()[0]
                            };
                        }
                    }
                    return null;
                })
                .Where(o => o != null)
                .ToList();

            foreach (var handlerDefinition in handlerDefinitions)
            {
                this.Register(handlerDefinition.NotificationType, handlerDefinition.HandlerType);
            }

            //Console.WriteLine(string.Format("[ Bus ] Subscription({0}): Registered handlers in assembly {1}", this.TopicName, assembly.GetName().Name));
            return this;
        }

        protected IDictionary<Type, IList<object>> Handlers => _handlers;

        protected void ExecuteHandler(Type messageType, object handlerObject, string text)
        {
            if (handlerObject is Type)
            {
                var handlerType = (Type)handlerObject;

                if (
                    handlerType.GetInterfaces()
                        .Where(t => t.IsGenericType)
                        .Where(t => t.GetGenericTypeDefinition() == typeof(IHandler<>))
                        .Any()
                )
                {
                    var resolve = _connection.Resolver;
                    var handler = resolve(handlerType);
                    var message = JsonConvert.DeserializeObject(text, messageType);
                    handlerType.InvokeMember("Execute", BindingFlags.Default | BindingFlags.InvokeMethod, null, handler, new object[] { message });
                    return;
                }

            }
            else if (handlerObject.GetType().IsGenericType && handlerObject.GetType().GetGenericTypeDefinition() == typeof(Action<>))
            {
                //var handler = (Action<TNotification>)o;
                //handler(notification);
            }
            throw new System.Exception($"Was unable to handle message {messageType.Name}");
        }

        public abstract void Initialise();

        public abstract void Dispose();
    }
}
