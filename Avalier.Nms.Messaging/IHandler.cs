﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Nms.Messaging
{
    public interface IHandler<TMessage>
        where TMessage : class
    {
        void Execute(TMessage message);
    }

    public interface IHandler<TRequest, TResponse>
        where TRequest : class
        where TResponse : class
    {
        TResponse Execute(TRequest request);
    }
}
