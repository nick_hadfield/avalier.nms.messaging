﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Nms.Messaging
{
    public class Bus : IBus
    {
        public Bus(IConnection connection)
        {
            this.Connection = connection;
        }

        public IConnection Connection { get; }

        public void Publish<TNotification>(TNotification notification) where TNotification : class
        {
        }

        public void Send(string queueName, string text)
        {
            var sender = this.Connection.GetSenderByQueue(queueName);
            if (null != sender)
            {
                sender.Send(text);
            }
        }

        public void Send<TCommand>(TCommand command) where TCommand : class
        {
            foreach (var sender in this.Connection.Senders)
            {
                sender.Send(command);
            }
        }

        public TResponse Request<TRequest, TResponse>(TRequest request) where TRequest : class where TResponse : class
        {
            return default(TResponse);
        }

        /*/
        public static Connection CreateConnection(string endpointName = "tcp://localhost:61616", string endpointUsername = "", string endpointPassword = "")
        {
            var connection = new BusConnection(endpointName, endpointUsername, endpointPassword);
            return connection;
        }
        //*/

        public void Dispose()
        {
            this.Connection.Dispose();
        }
    }
}
