﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avalier.Nms.Messaging;
using Avalier.Test.Contracts;

namespace Avalier.Test.Client
{
    public class FlipCommandHandler : IHandler<FlipCommand>
    {
        public void Execute(FlipCommand message)
        {
            Console.WriteLine("Handled FlipCommand");
        }
    }
}
