﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Avalier.Nms.Messaging;
using Avalier.Test.Contracts;

namespace Avalier.Test.Client
{
    public class TextCommandHandler : IHandler<TextCommand>
    {
        private const int SleepDuration = 0;
        private static int _messageCount = 0;

        public void Execute(TextCommand message)
        {
            _messageCount++;
            var startedAt = DateTime.UtcNow;
            var threadName = System.Threading.Thread.CurrentThread.Name;
            //Console.WriteLine($"Started work on thread {threadName}, message number {_messageCount}");
            System.Threading.Thread.Sleep(SleepDuration);
            var endedAt = DateTime.UtcNow;
            var elapsed = (long)endedAt.Subtract(startedAt).TotalMilliseconds;
            //Console.WriteLine($"Finished work on thread {threadName}, message number {_messageCount} in {elapsed} milliseconds");
        }
    }
}
