﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Avalier.Test.Contracts;
using Avalier.Nms.Messaging;

namespace Avalier.Test.Client
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var handles = new List<AutoResetEvent>();

            var connection = new Avalier.Nms.Messaging.ActiveMQ.Connection(
                endpointName: "tcp://localhost:61616?jms.prefetchPolicy.all=100",
                endpointUsername: "admin",
                endpointPassword: "admin",
                clientId: "Avalier.Test.Client"
            );

            connection
                //.AddSender(sender => sender.Scan(Assembly.Load("Avalier.Test.Contracts")))
                .AddReceiver("Avalier.Test", receiver => receiver.ScanForHandlers());

            using (var bus = connection.Start())
            {
                Console.WriteLine("Press <enter> to continue...");
                Console.ReadLine();
            }
        }

    }
}
