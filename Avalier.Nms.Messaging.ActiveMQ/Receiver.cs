﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Apache.NMS;
using Avalier.Nms.Messaging.ActiveMQ;
using Newtonsoft.Json;

namespace Avalier.Nms.Messaging.ActiveMQ
{
    public class Receiver : Base.Receiver
    {
        private readonly Connection _connection;
        private IMessageConsumer _messageConsumer;
        private MessageListener _messageListener;

        public Receiver(Connection connection, string queueName) : base(connection, queueName)
        {
            _connection = connection.ThrowIfNull(nameof(connection));
        }

        public override void Initialise()
        {
            var session = _connection.NetTxSession.ThrowIfNull("session");
            _messageConsumer = session.CreateConsumer(session.GetDestination(this.QueueName));
            _messageListener = new MessageListener(OnMessage);
            _messageConsumer.Listener += _messageListener;
        }

        private void OnMessage(IMessage message)
        {
            var textMessage = message as ITextMessage;
            if (null != textMessage)
            {
                var verb = textMessage.Properties[Magic.MessageHeader.Verb].ToString();
                var action = textMessage.Properties[Magic.MessageHeader.Action].ToString();
                var correlationId = textMessage.NMSCorrelationID;
                //Console.WriteLine($"{verb}:{action} [{correlationId}] {textMessage.Text}");
                Console.WriteLine($"{_connection.ClientName} - {verb}:{action} [{correlationId}]");

                var messageType = Type.GetType(action);
                if (base.Handlers.ContainsKey(messageType))
                {
                    foreach (var handlerObject in base.Handlers[messageType])
                    {
                        Console.WriteLine($"Calling handler {handlerObject.ToString()}");
                        this.ExecuteHandler(messageType, handlerObject, textMessage.Text);
                    }
                }
                
            }
        }

        public override void Dispose()
        {
            if (null != _messageConsumer)
            {
                _messageConsumer.Close();
                _messageConsumer.Listener -=_messageListener;
                _messageConsumer.Dispose();
                _messageConsumer = null;
                _messageListener = null;
            }
        }
    }
}
