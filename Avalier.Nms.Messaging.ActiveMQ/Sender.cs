﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Apache.NMS;
using Apache.NMS.Util;

namespace Avalier.Nms.Messaging.ActiveMQ
{
    public class Sender : Base.Sender, IDisposable
    {
        private readonly Connection _connection;                                      
        private IMessageProducer _messageProducer;

        internal Sender(Connection connection, string queueName) : base(queueName)
        {
            _connection = connection.ThrowIfNull(nameof(connection));
        }

        public override void Initialise()
        {
            var session = _connection.NetTxSession.ThrowIfNull("session");
            _messageProducer = session.CreateProducer(session.GetDestination(this.QueueName));
        }

        public override void Dispose()
        {
            if (null != _messageProducer)
            {
                _messageProducer.Close();
                _messageProducer.Dispose();
                _messageProducer = null;
            }
        }

        public override void Send(string text)
        {
                var session = _connection.NetTxSession.ThrowIfNull("session");

                // Start message (and populate/serialise body) //
                var message = session.CreateTextMessage(text);

                // Set headers //
                message.NMSCorrelationID = Guid.NewGuid().ToString();
                message.Properties[Magic.MessageHeader.Client] = _connection.ClientName;

                // Send message //
                _messageProducer.Send(message);
        }

        public  override void Send<TCommand>(TCommand command)
        {
            if (base.Supports<TCommand>())
            {
                var session = _connection.NetTxSession.ThrowIfNull("session");

                // Start message (and populate/serialise body) //
                var message = session.CreateTextMessage(command.ToJson());

                // Set headers //
                message.NMSCorrelationID = Guid.NewGuid().ToString();
                message.Properties[Magic.MessageHeader.Client] = _connection.ClientName;
                message.Properties[Magic.MessageHeader.Verb] = "Command";
                message.Properties[Magic.MessageHeader.Action] = typeof(TCommand).AssemblyQualifiedName
                    .Split(new[] { ", " }, StringSplitOptions.None)
                    .Take(2).Merge(", ");

                // Send message //
                _messageProducer.Send(message);
            }
        }
    }
}
