﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Avalier.Nms.Messaging
{
    internal static class Extensions
    {
        public static T ThrowIfNull<T>(this T value, string paramName)
            where T : class
        {
            if (null == value)
            {
                throw new System.ArgumentNullException(paramName);
            }
            return value;
        }

        public static string ThrowIfEmpty(this string value, string paramName)
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new System.ArgumentNullException(paramName);
            }
            return value;
        }

        public static string ToJson<T>(this T value)
        {
            return JsonConvert.SerializeObject(value);
        }

        public static string Merge(this IEnumerable<string> values, string seperator = ", ")
        {
            var sb = new StringBuilder();
            foreach (var s in values)
            {
                if (!string.IsNullOrEmpty(s))
                {
                    if (0 != sb.Length) sb.Append(seperator);
                    sb.Append(s);
                }
            }
            return sb.ToString();
        }
    }
}
