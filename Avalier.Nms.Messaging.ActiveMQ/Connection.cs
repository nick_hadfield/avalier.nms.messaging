﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Avalier.Nms.Messaging.ActiveMQ
{
    public class Connection : Base.Connection<Receiver, Sender>
    {
        public Connection(string endpointName, string endpointUsername, string endpointPassword, string clientId = "") : base(clientId)
        {
            this.EndpointName = endpointName.ThrowIfEmpty(nameof(endpointName));
            this.EndpointUsername = endpointUsername;
            this.EndpointPassword = endpointPassword;
        }

        public string EndpointName { get; }

        public string EndpointUsername { get; }

        public string EndpointPassword { get; }

        public override IConnection<Receiver, Sender> AddSender(string queueName, Action<Sender> senderConfiguration = null)
        {
            queueName.ThrowIfEmpty(nameof(queueName));
            var sender = new Sender(this, queueName);
            senderConfiguration?.Invoke(sender);
            this.SendersByQueue.Add(queueName, sender);
            return this;
        }

        public override IConnection<Receiver, Sender> AddReceiver(string queueName, Action<Receiver> receiverConfiguration = null)
        {
            queueName.ThrowIfEmpty(nameof(queueName));
            var receiver = new Receiver(this, queueName);
            receiverConfiguration?.Invoke(receiver);
            this.Receivers.Add(receiver);
            return this;
        }

        public Apache.NMS.INetTxConnection NetTxConnection { get; set; }

        public Apache.NMS.INetTxSession NetTxSession { get; set; }

        public override IBus Start()
        {
            var brokerUri = new Uri(this.EndpointName);
            var connectionFactory = new Apache.NMS.ActiveMQ.NetTxConnectionFactory(brokerUri);
            this.NetTxConnection = connectionFactory.CreateNetTxConnection(this.EndpointUsername, this.EndpointPassword);
            this.NetTxSession = this.NetTxConnection.CreateNetTxSession();
            foreach (var sender in ((IConnection)this).Senders) sender.Initialise();
            foreach (var receiver in ((IConnection)this).Receivers) receiver.Initialise();
            var bus = new Bus(this);
            this.NetTxConnection.Start();
            return bus;
        }

        public override void Dispose()
        {
            foreach (var receiver in ((IConnection)this).Receivers) receiver.Dispose();
            foreach (var sender in ((IConnection)this).Senders) sender.Dispose();
            this.NetTxSession?.Close();
            this.NetTxSession?.Dispose();
            this.NetTxConnection?.Stop();
            this.NetTxConnection?.Close();
            this.NetTxConnection?.Dispose();
        }
    }
}
